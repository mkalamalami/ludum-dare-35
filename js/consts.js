define([], function() {

	return {

		DEBUG: {},

		LEVEL_COUNT: 17,

		TILESIZE: 16,
		PIXEL_SCALE: 2,

		WIDTH: 960,
		HEIGHT: 576,

		GRAVITY: 1600,
		CIRCLE_SPEED: 160,
		CIRCLE_JUMP_SPEED: 410, // can jump 2 squares high
		CIRCLE_ACCELERATION: 120, // ms

		SQUARE_SPEED: 180,

		SAVENAME: 'save'
		
	};

});
