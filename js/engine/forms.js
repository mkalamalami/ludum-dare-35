define([], function() {

	const $phaser = $('#phaser');
	const $form = $('#phaser-form');

	var activeForm = null;

	init();

	// Form class

	const Form = function(id) {
		this.id = id;
		this.template = _.template($('#' + id).html());
	}
	Form.prototype.show = function(fadeDelay) {
		$form.fadeIn(fadeDelay || 0);
	}
	Form.prototype.hide = function(fadeDelay) {
		$form.fadeOut(fadeDelay || 0);
	}
	Form.prototype.getElement = function(id) {
		return $('#' + id, this.$el);
	}

	return {
		// Call after initializing Phaser to make sure the forms adapt to the game size & position.
		// refresh()
		refresh: refresh,

		// Loads a Form given its id and a context to provide to the template
		// load(id, vars)
		load: load,

		// Get the current form
		// getActive()
		getActive: getActive
	};

	// Functions

	function init() {
		$(window).resize(refresh);
	}

	function refresh() {
		$form.css('left', $phaser.position().left + ($phaser.width() - $form.width())/2 + 'px');
		$form.css('top', $phaser.position().top + ($phaser.height() - $form.height())/2 + 'px');
	}

	function load(id, vars) {
		activeForm = new Form(id);
		$form.html(activeForm.template(vars));
		refresh();
		return activeForm;
	}

	function getActive() {
		return activeForm;
	}

});