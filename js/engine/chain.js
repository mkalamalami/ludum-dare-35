define([], function() {
	
	var Chain = function(o) {
		this._killed = false;
		this._steps = [];
		this._currentStep = -1;
		this._bound = this;
		if (o) {
			this.bind(o);
		}
	}

	Chain.prototype.bind = function(o) {
		this._bound = o;
	}

	Chain.prototype.add = function(f) {
		this._steps.push(f);
	}

	Chain.prototype.start = function() {
		this._currentStep = -1;
		this.next();
	}

	Chain.prototype.next = function() {
		if (!this._killed) {
			this._currentStep++;
			if (this._steps.length > this._currentStep) {
				this._steps[this._currentStep].call(this._bound, 
					this._getCallback(this._currentStep));
			}
		}
		else {
			return function() {};
		}
	}

	Chain.prototype.kill = function() {
		this._killed = true;
	}

	Chain.prototype._getCallback = function(step) {
		var chain = this;
		return function() {
			if (step == chain._currentStep) { // prevent excessive calls
				chain.next();
			}
		}
	}

	return {

		create: function(o) {
			return new Chain(o);
		}
		
	}
})