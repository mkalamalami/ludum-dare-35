define(['consts'], function(Consts) {

	/**
	 * PhaserJS configuration & preloader launch.
	 */
	
	var state = new Phaser.State();

	state.preload = function() {
		this.load.image('preloader_bar', 'img/preloader_bar.png');
	},

	state.create = function() {

		// Unless you specifically know your game needs to support set this to 1
		this.input.maxPointers = 1;

		// Phaser will automatically pause if the browser tab the game is in loses focus.
		this.stage.disableVisibilityChange = true;

		if (this.game.device.desktop) {
			this.scale.pageAlignHorizontally = true;
		} else {
			this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
			/*this.scale.minWidth = MIN_WIDTH;
			this.scale.minHeight = MIN_HEIGHT;
			this.scale.maxWidth = MAX_WIDTH;
			this.scale.maxHeight = MAX_HEIGHT;*/
			this.scale.forceLandscape = true;
			this.scale.pageAlignHorizontally = true;
			this.scale.setScreenSize(true);
		}

		// Let's start the real preloader going
		this.state.start('preloader');
	};

	return state;

});
