define(['lib/moment.min'], function(moment) {

	var saveState = {};

	saveState.init = function() {
		_.each(saveState, function(value, key) {
			if (typeof (value) != "function") {
				delete saveState[key];
			}
		});

		// Initial data
		saveState.playerName = "New player";
	};

	saveState.save = function(otherName) {
		var data = {};
		_.each(saveState, function(value, key) {
			if (typeof (value) != "function") {
				data[key] = value;
			}
		});
		var gameName = otherName || saveState.gameName;
		$.jStorage.set(gameName, JSON.stringify(data));

		var savedGames = $.jStorage.get("savedGames");
		if (savedGames == undefined) {
			savedGames = [];
		}
		savedGames = _.without(savedGames, gameName);
		savedGames.unshift(gameName);
		$.jStorage.set("savedGames", savedGames);
	};

	saveState.removeSavedGame = function(gameName) {
		var savedGames = $.jStorage.get("savedGames");
		savedGames = _.without(savedGames, gameName);
		$.jStorage.set("savedGames", savedGames);
		$.jStorage.deleteKey(gameName);
	};

	saveState.findExistingGames = function(gameName) {
		var savedGames = $.jStorage.get("savedGames");
		if (savedGames == undefined) {
			savedGames = [];
		}
		return savedGames;
	};
	
	saveState.load = function(gameName) {
		var rawData = $.jStorage.get(gameName);
		if (rawData != null) {
			var data = JSON.parse(rawData);
			_.each(data, function(value, key) {
				saveState[key] = value;
			});
			saveState.date = moment(saveState.date);
		}
		return rawData != null;
	};

	return saveState;

});
