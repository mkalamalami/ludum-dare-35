define(['assets', 'consts', 'engine/forms'], function(Assets, Consts, Forms) {

	/**
	 * Preloading screen.
	 * Loads all assets, then launches the actual game.
	 */
	
	var state = new Phaser.State();

	state.preload = function () {
		this.ready = false;
		
		state.physics.startSystem(Phaser.Physics.ARCADE);
		
		//	These are the assets we loaded in Boot.js
		//	A nice sparkly background and a loading progress bar
		/*this.background = this.add.sprite(this.world.centerX, this.world.centerY - 100, 'preloader_background');
		this.background.anchor.set(.5);*/
		this.preloadBar = this.add.sprite(this.world.centerX, this.world.centerY + 150, 'preloader_bar');
		this.preloadBar.anchor.set(.5);

		//	This sets the preloadBar sprite as a loader sprite.
		//	What that does is automatically crop the sprite from 0 to full-width
		//	as the files below are loaded in.
		this.load.setPreloadSprite(this.preloadBar);
		
		preloadImageFolder('img/', Assets.images);
		preloadAudioFolder('audio/', Assets.audio);
		preloadSpritesheetFolder('spritesheet/', Assets.spritesheet);

		// Additional pre-loading
		[16,32].forEach(function(size) {
			state.load.bitmapFont('visitor' + size, 'fonts/visitor' + size + '.png', 'fonts/visitor' + size + '.fnt');
		});
	};

	state.create = function () {

		//	Once the load has finished we disable the crop because we're going to sit in the update loop for a short while as the music decodes
		this.preloadBar.cropEnabled = false;

	};

	state.update = function () {

		//	You don't actually need to do this, but I find it gives a much smoother game experience.
		//	Basically it will wait for our audio file to be decoded before proceeding to the MainMenu.
		//	You can jump right into the menu if you want and still play the music, but you'll have a few
		//	seconds of delay while the mp3 decodes - so if you need your music to be in-sync with your menu
		//	it's best to wait for it to decode here first, then carry on.
		
		//	If you don't have any music in your game then put the game.state.start line into the create function and delete
		//	the update function completely.
		
		// if (this.cache.isSoundDecoded('titleMusic') && this.ready == false)
		// {
		this.ready = true;
		this.state.start('main');
		// }
	};
	
	return state;
	
	// ===== PRELOADERS  =====
	
	function preloadImageFolder(folderName, assets) {
		assets.forEach(function(asset) {
			if (typeof asset == 'string') {
				var assetPath = folderName + asset + ((asset.indexOf('.') == -1) ? '.png' : '');
				state.load.image(asset, assetPath);
			}
			else if (typeof asset == 'object') {
				preloadImageFolder(folderName + asset[0], _.tail(asset));
			}
			else {
				console.error("Invalid asset type: " + (typeof asset));
				console.error(asset);
			}
		});
	}
	
	function preloadAudioFolder(folderName, assets) {
		assets.forEach(function(asset) {
			if (typeof asset == 'string') {
				var assetPath = folderName + asset + ((asset.indexOf('.') == -1) ? '.mp3' : '');
				state.load.audio(asset, [assetPath]);
			}
			else if (typeof asset == 'object') {
				preloadAudioFolder(folderName + asset[0], _.tail(asset));
			}
			else {
				console.error("Invalid asset type: " + (typeof asset));
				console.error(asset);
			}
		});
	}
	
	function preloadSpritesheetFolder(folderName, assets) {
		assets.forEach(function(asset) {
			var assetPath = folderName + asset[0] + ((asset[0].indexOf('.') == -1) ? '.png' : '');
			state.load.spritesheet(asset[0], assetPath, asset[1], asset[2]);
		});
	}
});


