require.config({
	baseUrl: "./js",
	paths: {
		json3: "lib/json3.min", // JStorage requires JSON2/3
		jquery: "lib/jquery.min", // Bootstrap & "forms" require jQuery
		underscore: "lib/underscore.min", // Underscore (JS toolkit)
		jstorage: "lib/jstorage.min", // JStorage (local storage)
		optional: "lib/require-optional", // Support optional libraries flag in RequireJS
		phaser: "lib/phaser", // PhaserJS (game library)
	}
});

require(['json3', 'jquery', ], function() {
	require(['underscore', 'jstorage', 'phaser', 'optional'], function() {
		require(['consts', 'engine/state-boot', 'engine/state-preloader', 'state-main', 'optional!consts-debug'],
			function(Consts, StateBoot, StatePreloader, StateMain) {

			new Phaser.Game(Consts.WIDTH/Consts.PIXEL_SCALE, Consts.HEIGHT/Consts.PIXEL_SCALE, Phaser.AUTO, 'phaser', {
				create: function() {

					this.state.add('boot', StateBoot);
					this.state.add('preloader', StatePreloader);
					this.state.add('main', StateMain);

					this.state.start('boot');
				}
			});

		});
	});

	$(window).keydown(function(event) {
		if (event.which >= 65 /* a */ && event.which <= 90 /* z */
				|| event.which >= 37 /* left */ && event.which <= 40 /* down */
				|| event.which >= 32 /* space */) {
			event.preventDefault();
		}
	})
});
