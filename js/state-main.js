define(['consts', 'engine/save-state', 'state-level', 'state-intro', 'state-resume', 'state-outro'],
		function(Consts, SaveState, StateLevel, StateIntro, StateResume, StateOutro) {

	var state = new Phaser.State();

	state.preload = function() {

		// Game configuration

		this.game.scale.scaleMode = Phaser.ScaleManager.USER_SCALE;  
		this.game.scale.setUserScale(Consts.PIXEL_SCALE, Consts.PIXEL_SCALE);
		this.game.renderer.renderSession.roundPixels = true;  
		this.game.antialias = false;
		Phaser.Canvas.setImageRenderingCrisp(this.game.canvas);
		
		this.game.world.setBounds(0, 0,
			Consts.WIDTH/Consts.PIXEL_SCALE, Consts.HEIGHT/Consts.PIXEL_SCALE);
		this.game.physics.startSystem(Phaser.Physics.P2JS);
		this.game.physics.p2.setImpactEvents(true);
		this.game.physics.p2.restitution = 0;

		// Register game states

		this.state.add('intro', StateIntro);
		this.state.add('resume', StateResume);
		this.state.add('level', StateLevel);
		this.state.add('outro', StateOutro);

		// Load & start

		if (Consts.DEBUG.TEST_OUTRO) {
			this.state.start('outro');
		}
		else if (!SaveState.load(Consts.SAVENAME) || !SaveState.level) {
			SaveState.init();
			SaveState.level = 1;
			this.state.start('intro');
		}
		else {
			this.state.start('resume');
		}

	};

	state.create = function() {

	};

	return state;

});
