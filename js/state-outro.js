define(['consts', 'engine/save-state', 'engine/chain'],
		function(Consts, SaveState, Chain) {

	var state = new Phaser.State();

	var achievements;

	state.create = function() {
		achievements = this.add.group();

		var chain = Chain.create(this);

		chain.add(delay(1000));
		chain.add(text(this, 76, 30, '...you win the game.'));
		chain.add(delay(2000));
		for (var i = 1; i <= 17; i++) {
			chain.add(text(this, 75, 50 + i * 12, 'Achievement get: Level ' + i + ' completed', 16));
			chain.add(delay(i == 1 ? 2000 : 350 - i * 20));
		}
		chain.add(delay(1000));
		chain.add(text(this, 75, 50 + 18 * 12, 'Achievement get: Game completed', 16));
		chain.add(delay(2000));
		chain.add(text(this, 150, 150, 'Thanks for playing', 16));
		chain.add(function() {
			achievements.visible = false;
		});
		chain.start();
	};

	function delay(delay) {
		return function(next) {
			setTimeout(next, delay);
		}
	}

	function text(state, x, y, label, size, align) {
		return function(next) {
			var entity = state.add.bitmapText(x, y, 'visitor' + (size || 32), label, size || 32);
			entity.align = align || 'left';
			if (Consts.DEBUG.TEXT) {
				entity.inputEnabled = true;
				entity.input.enableDrag(true);
			    entity.update = function() {
			    	if (entity._logPosition) {
			    		console.log(Math.round(entity.x) + "," + Math.round(entity.y) 
			    				+ " (" + entity.text + ")");
			    	}
			    }
			    entity.events.onDragStart.add(function() {
			    	entity._logPosition = true;
			    }, this);
			    entity.events.onDragStop.add(function() {
			    	entity._logPosition = false;
			    }, this);
			}

			if (label.indexOf('Achievement get') != -1) {
				achievements.add(entity);
			}

			next();
		}
	}

	return state;

});
