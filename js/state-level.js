define(['consts', 'engine/save-state', 'entities/tilemap', 'entities/player', 'level-text'],
		function(Consts, SaveState, Tilemap, Player, LevelText) {

	var state = new Phaser.State();

	state.preload = function() {
		if (SaveState.level > Consts.LEVEL_COUNT) {
			SaveState.level = 1;
		}

		if (!Consts.DEBUG.FORCE_LEVEL) {
			SaveState.save(Consts.SAVENAME);
		}
		else {
			SaveState.level = Consts.DEBUG.FORCE_LEVEL;
		}

		this.load.tilemap(SaveState.level, 'levels/' + SaveState.level + '.json',
			null, Phaser.Tilemap.TILED_JSON);
	};

	state.create = function() {
		this._bg1 = this.add.tileSprite(0, 0, Consts.WIDTH, Consts.HEIGHT, 'background_1');
		this._bg2 = this.add.tileSprite(0, 0, Consts.WIDTH, Consts.HEIGHT, 'background_1');
		this._bg2.alpha = 0.5;
		this._bg2.tilePosition.x = this._bg2.width / 2;
		this._bg2.tilePosition.y = this._bg2.height / 2;

		this._map = Tilemap.load(this, SaveState.level);
		this._player = this._map._player;
		this._alternatePlayer = this._map._alternatePlayer;
		if (this._alternatePlayer != null) {
			this._alternatePlayer.disable();
		}
	    this.refreshLevelColors();

	    LevelText.create(state, SaveState.level);

	    if (Consts.DEBUG.TEXT) {
	    	state.input.mouse.capture = true;
	    }
	};

	state.update = function()  {
		// Background animation
		this._bg1.tilePosition.x += .25;
		this._bg1.tilePosition.y += (this._player._isSquare) ? -.5 : .5;
		this._bg2.tilePosition.x -= .2;
		this._bg2.tilePosition.y += (this._player._isSquare) ? -.3 : .3;

	    // Movement
	    if (this.input.keyboard.isDown(Phaser.Keyboard.RIGHT) || this.input.keyboard.isDown(Phaser.Keyboard.D)) {
	    	if (!this._holdingRightSince) {
	    		this._holdingRightSince = this.time.time;
	    	}
	    	this._player.goRight(this._holdingRightSince);
	    }
	    else {
	    	this._holdingRightSince = false;
	    }
	    if (this.input.keyboard.isDown(Phaser.Keyboard.LEFT) || this.input.keyboard.isDown(Phaser.Keyboard.Q) || this.input.keyboard.isDown(Phaser.Keyboard.A)) {
	    	if (!this._holdingLeftSince) {
	    		this._holdingLeftSince = this.time.time;
	    	}
	    	this._player.goLeft(this._holdingLeftSince);
	    }
	    else {
	    	this._holdingLeftSince = false;
	    }

	    if (this.input.keyboard.isDown(Phaser.Keyboard.UP) || this.input.keyboard.isDown(Phaser.Keyboard.Z) || this.input.keyboard.isDown(Phaser.Keyboard.W)) {
	    	if (!this._isHoldingUp || this._player._isSquare) {
	    		var success = this._player.goUp(this);
	    		if (success) {
	    			this._isHoldingUp = true;
	    		}
	    	}
	    }
	    else {
	    	this._isHoldingUp = false;
	    }

	    if (this.input.keyboard.isDown(Phaser.Keyboard.DOWN) || this.input.keyboard.isDown(Phaser.Keyboard.S)) {
	    	this._player.goDown();
	    }

	    // Shapeshift
	    if (this.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
	    	if (!this._isHoldingSpace) {
	    		if (this._alternatePlayer) {
	    			var buffer = this._player;
	    			this._player = this._alternatePlayer;
					this._player.enable();
	    			this._alternatePlayer = buffer;
					this._alternatePlayer.disable();
	    		}
	    		else {
		    		this._player.shapeShift();
		    	}
	    	}
	    	this._isHoldingSpace = true;
	    }
	    else {
	    	this._isHoldingSpace = false;
	    }
	    this.refreshLevelColors();

	    // Merge into a squircle
	    if (this._player._merge) {
	    	if (this._alternatePlayer) {
		    	var x = this._player.x, y = this._player.y, type = this._player._type;
		    	this._alternatePlayer.destroySensors();
		    	this._alternatePlayer.destroy();
		    	this._player.destroySensors();
		    	this._player.destroy();
		    	this._alternatePlayer = null;
				this._player = Player.create(state, this._map, x, y, type, false);
		    	if (this._specialText) {
		    		this._specialText.visible = true;
		    	}
		    }
		    else {
		    	// Endgame when merging with a triangle
		    	this.state.start('outro');
		    	return;
		    }
	    }

	    // Refresh level
	    if (this.input.keyboard.isDown(Phaser.Keyboard.R)) {
	    	this._player.reset();
	    }

	    // Level end
	    if (this._player.hasFinishedLevel()) {
	    	if (this._alternatePlayer) {
	    		this._player.destroySensors();
	    		this._player.destroy();
	    		this._player = this._alternatePlayer;
	    		this._alternatePlayer = null;
				this._player.enable();
	    	}
	    	else {
	    		Consts.DEBUG.FORCE_LEVEL = false;
		    	SaveState.level++;
		    	this._levelType = null;
		    	this.state.start('level');
		    }
	    }
	}

	state.refreshLevelColors = function() {
		var type = this._player._type;
		if (type != this._levelType) {
			this._bg1.loadTexture('background_' + this._map._backgrounds[type]);
			this._bg2.loadTexture('background_' + this._map._backgrounds[type]);
			this._map.switchTilesColor(type);
			this._levelType = type;
		}
	}

	return state;

});
