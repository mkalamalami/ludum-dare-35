define(['consts', 'engine/save-state', 'engine/chain'],
		function(Consts, SaveState, Chain) {

	var state = new Phaser.State();

	state.create = function() {
		if (SaveState.playedIntro && !Consts.DEBUG.FORCE_INTRO || Consts.DEBUG.SKIP_INTRO) {
			this.state.start('level');
			return;
		}

		var chain = Chain.create(this);
		chain.add(text(this, 'Loading uninspired platformer gameplay', 'OK'));
		chain.add(function(next) {
			SaveState.playedIntro = true;
			SaveState.level = 1;
			SaveState.save(Consts.SAVENAME);
			next();
		});
		chain.add(text(this, 'Loading original mechanic to fit LD35', 'OK'));
		chain.add(text(this, 'Loading twist ending', 'OK'));
		chain.add(text(this, 'Loading state-of-the-art graphics'));
		chain.add(text(this, '  CRITICAL FAILURE'));
		chain.add(text(this, '  WILL ATTEMPT FALLBACK'));
		chain.add(text(this, 'Replacing graphics with squares and circles', 'OK'));
		chain.add(text(this, 'Personifying shapes using narrative text', 'OK'));
		chain.add(text(this, 'Loading sounds and music'));
		chain.add(text(this, '  CRITICAL FAILURE'));
		chain.add(text(this, 'Giving up', 'OK'));
		chain.add(text(this, 'Loading complete.'));
		chain.add(function() {
			SaveState.playedIntroOnce = true;
			this.state.start('level');
		});

		chain.start();
	};

	var x = 10, y = 10, dy = 15;
	var REPLAY_SPEEDUP = .1;

	function text(state, label, status) {
		return function(next) {
			var text = state.add.bitmapText(x, y, 'visitor16', label, 16);
			y += dy;
			if (status) {
				dots(text, 10, function() {
					text.text += ' ' + status;
					setTimeout(function() {
						next();
					}, 600 * (SaveState.playedIntroOnce ? REPLAY_SPEEDUP : 1));
				});
			}
			else {
				setTimeout(function() {
					next();
				}, label.length * 65 * (SaveState.playedIntroOnce ? REPLAY_SPEEDUP : 1));
			}
		}
	}

	function dots(text, max, callback, i) {
		i = i || 0;
		setTimeout(function() {
			text.text += '.';
			if (i == max) {
				callback();
			}
			else {
				dots(text, max, callback, i+1);
			}
		}, text.text.length * 5 * (SaveState.playedIntroOnce ? REPLAY_SPEEDUP : 1));
	}

	return state;

});
