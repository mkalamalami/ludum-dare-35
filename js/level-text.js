define(['consts'], function(Consts) {

	return {
		create: create,
	};

	// Sizes available: 16, 32

	function create(state, levelId) {

		switch (levelId) {

		case 1:
			setTimeout(function() {
				text(state, 28, 18, 'circle is a circle.');
				text(state, 28, 47, 'she can walk and jump, like you would expect\nfrom any respectable circle.', 16);
			}, 500);
			break;

		case 2:
			text(state, 115, 184, 'square is a square.', 32);
			text(state, 144, 217, 'he slides in all directions, with a\ncomplete disregard for gravity.', 16, 'right');
			text(state, 157, 255, '> press R to restart any level', 16);
			break;

		case 3:
			text(state, 27, 173, 'circle wants someone\nin her life.', 16, 'right');
			text(state, 22, 217, 'she wants someone to', 16);
			text(state, 142, 230, 'love.', 32);
			break;

		case 4:
			text(state, 20, 19, 'square too could use\nsome company...', 16);
			text(state, 20, 62, 'he\'d really\nlike some', 16);
			text(state, 20, 88, 'affection.', 32);
			break;

		case 5:
			text(state, 12, 175, 'they are both sad right now, but', 16);
			text(state, 12, 197, 'maybe they\'ll soon\nmeet each other.', 32);
			text(state, 12, 264, '> press space to toggle between them', 16);
			break;

		case 6:
			text(state, 23, 129, 'circles and squares are meant for each other.', 16);
			break;

		case 7:
			text(state, 185, 165, 'and when a square and a circle\nlove each other very much...', 16);
			state._specialText = state.add.group();
			state._specialText.add(text(state, 23, 219, 'They make a squircle.', 32));
			state._specialText.add(text(state, 23, 252, '> press Space to shapeshift', 16));
			state._specialText.visible = false;
			break;

		case 8:
			text(state, 16, 12, 'the squircle is extatic.', 32);
			text(state, 16, 44, 'circle and square could never do\nsuch things on their own.', 16, 'left');
			text(state, 81, 178, '1 + 1 = 1', 16, 'right');
			break;

		case 9:
			text(state, 140, 213, 'what was once so\nhard to achieve...', 32);
			break;

		case 10:
			text(state, 263, 89, 'could now\nbe done\npainlessly.', 32);
			state._specialText = text(state, 264, 187, '(or not...)', 16);
			state._specialText.visible = false;
			break;

		case 11:
			text(state, 14, 13, 'so this squircle starts\ndoing crazy stuff.', 32);
			state._specialText = text(state, 264, 187, '(or not...)', 16);
			state._specialText.visible = false;
			break;

		case 12:
			text(state, 198, 13, 'and then even\nmore crazy\nstuff.', 32);
			text(state, 212, 257, 'except that at some point...', 16);
			break;

		case 13:
			text(state, 14, 10, '> press R to restart the level', 16);
			state._specialText = text(state, 99, 263, '...the squircle feels lonely.', 16);
			break;

		case 14:
			text(state, 69, 146, 'the squircle too\nneeds someone\nin its life.', 16, 'right');
			text(state, 29, 194, 'needs love.', 32);
			text(state, 36, 229, 'it feels lonely now,\nbut things are\nabout to change...', 16, 'right');
			break;

		case 15:
			state._specialText = text(state, 72, 259, 'the squircle will soon meet someone.\nit can feel it.', 16, 'center');
			break;

		case 16:
			state._specialText = text(state, 113, 178, 'squircles and triangles\nare meant for each other.', 16, 'center');
			break;

		case 17:
			state._specialText = text(state, 108, 210, 'and when a squircle and a triangle\nlove each other very much...', 16, 'right');
			break;
		}

	}

	function text(state, x, y, label, size, align) {
		var entity = state.add.bitmapText(x, y, 'visitor' + (size || 32), label, size || 32);
		entity.align = align || 'left';
		if (Consts.DEBUG.TEXT) {
			entity.inputEnabled = true;
			entity.input.enableDrag(true);
		    entity.update = function() {
		    	if (entity._logPosition) {
		    		console.log(Math.round(entity.x) + "," + Math.round(entity.y) 
		    				+ " (" + entity.text + ")");
		    	}
		    }
		    entity.events.onDragStart.add(function() {
		    	entity._logPosition = true;
		    }, this);
		    entity.events.onDragStop.add(function() {
		    	entity._logPosition = false;
		    }, this);
		}
		return entity;
	}

});
