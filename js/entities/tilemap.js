define(['consts', 'engine/save-state', 'entities/player', 'entities/prop', 'entities/triggers'],
		function(Consts, SaveState, Player, Prop, Triggers) {

	return {
		load: load
	};

	function load(state, tilemapId) {
		var map = state.add.tilemap(tilemapId);
		var backgroundGroup = state.add.group();
		var foregroundGroup = state.add.group();

		// Find the tile IDs requiring collisions
		var collisionTiles = [];
		map.tilesets.forEach(function(tileset, index) {
			if (tileset.tileProperties) {
				tileset.setImage(state.cache.getImage('tileset'));
				_.each(tileset.tileProperties, function(prop, key) {
					if (prop.TYPE) {
						collisionTiles.push(parseInt(key) + tileset.firstgid);
					}
				});
			}
		});
		
		// Prepare collision groups
		state._groups = {
			map: state.game.physics.p2.createCollisionGroup(),
			player: state.game.physics.p2.createCollisionGroup(),
			props: state.game.physics.p2.createCollisionGroup(),
			triggers: state.game.physics.p2.createCollisionGroup(),
			sensors: state.game.physics.p2.createCollisionGroup()
		};

		// Create layers
		var objects = [];
		map._layers = [];
   		map._halfWalls = [];
   		map._wallsMaterial = state.physics.p2.createMaterial('wallsMaterial');
		map.layers.forEach(function(layerInfo) {
			for (var i = 0; i < layerInfo.data.length; i++) {
				for (var j = 0; j < layerInfo.data[0].length; j++) {
					var tile = layerInfo.data[i][j];
					if (tile.properties.TYPE) {
						if (tile.properties.TYPE != 'wall') {
							objects.push({
								x: tile.x * tile.width,
								y: tile.y * tile.height,
								frame: tile.index - 1,
								properties: tile.properties
							});
							tile.index = -1;
						}
						else if (tile.properties.HALF) {
							tile._index = tile.index;
							tile.index = -1;
							map._halfWalls.push(tile);
						}
						else {
							tile.index = parseInt(map.properties.COLOR);
						}
					}
				}
			}

			var layer = map.createLayer(layerInfo.name);
			map.setCollision(collisionTiles, true, layer);

			var bodies = state.game.physics.p2.convertTilemap(map, layer);
			bodies.forEach(function(body) {
				body.setCollisionGroup(state._groups.map);
				body.collides(state._groups.player);
				body.collides(state._groups.props);
				body.collides(state._groups.sensors);
				body.kinematic = false;
				body.setMaterial(map._wallsMaterial);
				body._isMap = true;
			});

			if (Consts.DEBUG.SHOW_WORLD_BODIES) {
				layer.debug = true;
			}

			map._layers.push(layer);
		});
        state.game.physics.p2.updateBoundsCollisionGroup();

        // Half walls
   		map._halfWalls.forEach(function (halfWallData) {
   			var wall = state.add.sprite(
   				Math.floor(halfWallData.x * Consts.TILESIZE * 1.01),
   				halfWallData.y * Consts.TILESIZE,
   				'tileset',
   				halfWallData._index - 1);
			state.physics.p2.enable(wall, Consts.DEBUG.SHOW_WORLD_BODIES);
			wall.body.static = true;
			wall.body.x += wall.width/2;
			wall.body.y += wall.height/2;
   			if (halfWallData.properties.HALF == 'left') {
   				wall.body.setRectangle(wall.width/2, wall.height, -wall.width/4, 0);
   			}
   			else {
   				wall.body.setRectangle(wall.width/2, wall.height, wall.width/4, 0);
   			}
			wall.body.setCollisionGroup(state._groups.map);
			wall.body.collides(state._groups.player);
			wall.body.collides(state._groups.props);
			wall.body.collides(state._groups.sensors);
			wall.body.setMaterial(map._wallsMaterial);
			wall.body._isMap = true;
   		})


		// Additional functions & attributes
		map.randomizePropsSpeed = function() {
			map._props.forEach(function (prop) {
				prop.randomizeSpeed();
			});
		}

		var mapLayerId = map.getLayer('map');
		map.switchTilesColor = function(type) {
			var mapData = map._layers[mapLayerId].layer.data;
			var newId = map._tileColors[type];
			var oldId = (type == 'square') ? map._tileColors.circle : map._tileColors.square;
			for (var i = 0; i < mapData.length; i++) {
				for (var j = 0; j < mapData[0].length; j++) {
					if (mapData[i][j].index == oldId) {
						mapData[i][j].index = newId;
					}
				}
			}
			map._layers[mapLayerId].dirty = true;
		}
		
		// Create objects
		map._props = [];
		objects.forEach(function(object) {
			var objectType = object.properties.TYPE;
			if (objectType == 'square' || objectType == 'circle' || objectType == 'triangle') {
				if (map._player && objectType != 'triangle') {
					map._alternatePlayer = map._player;
				}
				var p = Player.create(state, map, object.x, object.y,
					objectType, map.properties.NOPOWER);
				if (objectType != 'triangle') {
					map._player = p;
					foregroundGroup.add(map._player);
				}		
				if (map._alternatePlayer && map._player._isSquare) {
					var buffer = map._player;
					map._player = map._alternatePlayer;
					map._alternatePlayer = buffer;
					map._player.enable();
				}

				map._backgrounds = {
					square: map.properties.BG,
					circle: map.properties.ALT_BG
				}

				map._tileColors = {
					square: parseInt(map.properties.COLOR),
					circle: parseInt(map.properties.COLOR)+8
				}
			}
			else if (objectType == 'prop') {
				var prop = Prop.create(state, parseInt(map.properties.COLOR), object.x, object.y)
				backgroundGroup.add(prop);
				map._props.push(prop);
			}
			else if (objectType == 'spike') {
				Triggers.createSpike(state, object.frame, object.x, object.y, object.properties.TIGHT)
			}
			else if (objectType == 'end') {
				Triggers.createEnd(state, object.x, object.y)
			}
			else if (objectType == 'reveal') {
				Triggers.createReveal(state, object.x, object.y)
			}
			else {
				console.error('Unknown object type: ' + objectType);
			}
		});

		if (!map._player) {
			throw new Error('level has no spawn, lol!');
		}

        return map;
    }

});
