define(['consts', 'engine/save-state', 'entities/player', 'entities/prop', 'entities/triggers'],
		function(Consts, SaveState, Player, Prop, Triggers) {

	return {
		playImplosion: playImplosion,
		playExplosion: playExplosion
	};

	function playImplosion(state, player) {
		for (var i = 0; i < 10; i++) {
			var p = state.add.sprite(
				player.x + 100 * (Math.random() - .5),
				player.y + 100 * (Math.random() - .5),
				'particle');
			p.alpha = 0;
			p._alphaIncrement = Math.random() / 10;
			p.update = function() {
				// movement
				this.x = (this.x * 5 + player.x) / 6;
				this.y = (this.y * 5 + player.y) / 6;

				// alpha
				this.alpha += this._alphaIncrement + 0.1;
				if (this.alpha >= 1) {
					this.destroy();
				}
			}
		}
    }

	function playExplosion(state, x, y) {
		var applyGravity = state._player._isCircle;
		for (var i = 0; i < 10; i++) {
			var p = state.add.sprite(
				x + 16 * (Math.random() - .5),
				y + 16 * (Math.random() - .5),
				'particle');
			p._alphaIncrement = Math.random() / 10;
			p._speedX = 10 * (Math.random() - .5);
			p._speedY = 10 * (Math.random() - .5);
			p._shined = false;
			p.update = function() {
				// movement
				this.x += this._speedX;
				this.y += this._speedY;

				// particle gravity
				if (applyGravity) {
					this._speedY += 1;
				}

				// alpha
				if (this._shined) {
					this.alpha -= .1;
					if (this.alpha <= 0) {
						this.destroy();
					}
				}
				else {
					this.alpha += this._alphaIncrement + 0.03;
					if (this.alpha >= 1) {
						this._shined = true;
					}
				}
			}
		}
    }

});
