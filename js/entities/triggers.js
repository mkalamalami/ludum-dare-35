define(['consts'], function(Consts) {

	return {
		createSpike: createSpike,
		createEnd: createEnd,
		createReveal: createReveal
	};

	function createSpike(state, frame, x, y, tight) {
		var entity = state.add.sprite(x, y, 'tileset', frame);

		state.physics.p2.enable(entity, Consts.DEBUG.SHOW_OBJECT_BODIES);
		if (frame == 25 && tight) { // upwards spike
			entity.body.clearShapes();
			entity.body.addPolygon({}, [ 
					[0, 12],
					[6, 2],
					[12, 12]
				]);
			entity.anchor.y = 0.7;
			entity.body.y += 4;
			entity.body.x += 2.5;
		}
		else {
			entity.body.y += entity.width/2+1;
			entity.body.x += entity.height/2+1;
		}
		entity.body.setCollisionGroup(state._groups.triggers);
		entity.body.collides(state._groups.player);
		entity.body.static = true;
		entity.body._isSpike = true;

		return entity;

	}

	function createEnd(state, x, y) {
		var entity = state.add.sprite(x, y);
		entity.width = Consts.TILESIZE;
		entity.height = Consts.TILESIZE;

		state.physics.p2.enable(entity, Consts.DEBUG.SHOW_OBJECT_BODIES);
		entity.body.setCollisionGroup(state._groups.triggers);
		entity.body.collides(state._groups.player);
		entity.body.x += Consts.TILESIZE / 2 + 1;
		entity.body.y += Consts.TILESIZE / 2 + 1;
		entity.body.static = true;
		entity.body._isEnd = true;

		return entity;
	}

	function createReveal(state, x, y) {
		var entity = state.add.sprite(x, y);
		entity.width = Consts.TILESIZE;
		entity.height = Consts.TILESIZE;

		state.physics.p2.enable(entity, Consts.DEBUG.SHOW_OBJECT_BODIES);
		entity.body.setCollisionGroup(state._groups.triggers);
		entity.body.collides(state._groups.player);
		entity.body.x += Consts.TILESIZE / 2 + 1;
		entity.body.y += Consts.TILESIZE / 2 + 1;
		entity.body.static = true;
		entity.body._isReveal = true;

		return entity;
	}

});
