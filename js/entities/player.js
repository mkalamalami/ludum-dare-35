define(['consts', 'entities/sensor', 'entities/particles'], function(Consts, Sensor, Particles) {

	return {
		create: create
	};

	function create(state, map, x, y, type, disableShapeShift) {

		var entity = state.add.sprite(x, y, type);
		entity._canShapeShift = !disableShapeShift;

		// Attach methods

		entity.setType = function(type) {
			if (type != 'circle' && type != 'square' && type != 'triangle') {
				throw new Error('invalid player type: ' + type);
			}
			this._type = type;
			this._isSquare = type == 'square';
			this._isCircle = type == 'circle';
			this._isTriangle = type == 'triangle';
			this._refreshPhysics();
			if (this._isSquare) {
				map.randomizePropsSpeed();
			}
		}

		entity._refreshPhysics = function() {
			if (this._currentPhysicsType != this._type) {
				if (this._isCircle) {
					this.loadTexture('circle');
					this.scale.set(1);
					this.body.setCircle(8, 0, 0);
					this.body.damping = 0.8;
					this.body.angularDamping = 0.999;
					this.body.inertia = 0;
					this.body.setMaterial(getCircleMaterial(state, map._wallsMaterial));
					state.physics.p2.gravity.y = Consts.GRAVITY;
					this.alpha = 1;

					if (this._currentPhysicsType) { // was circle
						this.body.velocity.x *= 1.5;
						if (this.body.velocity.y < 0) {
							this.body.velocity.y *= 4;
						}
					}
				}
				else if (this._isSquare) {
					this.loadTexture('square');
					this.scale.set(0.98);
					this.rotation = 0;
					this.body.setRectangle(entity.width, entity.height, 0, 0);
					this.body.x = Math.round(this.body.x / Consts.TILESIZE) * Consts.TILESIZE + 1;
					this.body.y = Math.round(this.body.y / Consts.TILESIZE) * Consts.TILESIZE + 1;
					this.body.rotation = 0;
					this.body.fixedRotation = true;
					this.body.setMaterial(getSquareMaterial(state, map._wallsMaterial));
					state.physics.p2.gravity.y = 0;

					// Momentum
					var dx = this.body.velocity.x, dy = this.body.velocity.y;
					var adx = Math.abs(dx), ady = Math.abs(dy);
					if (this._currentPhysicsType && 
							(adx > Consts.CIRCLE_SPEED/6 || ady > Consts.CIRCLE_SPEED/6
							|| !this._isCollidingAnything(state))) {
						this.alpha = 0.8;
						this._isSquareMoving = true;
						if (adx > ady) {
							this._squareDirection = [dx/adx,0];
						}
						else {
							this._squareDirection = [0,dy/ady];
						}
					}
					else {
						this._isSquareMoving = false;
						this._squareDirection = [0,0];
					}
				}
				else if (this._isTriangle) {
					this.loadTexture('triangle');
					this.scale.set(1);
					this.body.clearShapes();
					this.body.addPolygon({}, [ 
							[0, 32],
							[16, 5],
							[32, 32]
						]);
					this.body.damping = 0.8;
					this.body.angularDamping = 0.999;
					this.body.inertia = 0;
					this.body.setMaterial(getSquareMaterial(state, map._wallsMaterial));
					this.alpha = 1;
				}
				this.body.setCollisionGroup(state._groups.player);
				this.body.collides(state._groups.map);
				this.body.collides(state._groups.triggers, this.trigger, this);
				this.body.collides(state._groups.player, this.markForMerge, this);
				this.body.collideWorldBounds = true;
				this._currentPhysicsType = this._type;
			}
		}

		entity.enable = function() { // called on 2 players levels
			this.alpha = 1.0;
			if (this._isCircle) {
				state.physics.p2.gravity.y = Consts.GRAVITY;
			}
			else if (this._isSquare) {
				state.physics.p2.gravity.y = 0;
			}
		}

		entity.disable = function() {
			this.alpha = 0.7;
		}


		entity.markForMerge = function() {
			this._canShapeShift = true;
			this._merge = true;
		}

		entity.destroySensors = function() {
			this._squareSensors.forEach(function(sensor) {
				sensor.destroy();
			});
			this._circleSensors.forEach(function(sensor) {
				sensor.destroy();
			});
		}

		entity.update = function() {
			if (this._isSquare) {

				/*console.log("---");
				this._squareSensors.forEach(function(sensor) {
					console.log(sensor._name + ", " +sensor.isColliding())
				});*/

				if (this._isSquareMoving) {
					this.body.velocity.x = this._squareDirection[0] * Consts.SQUARE_SPEED;
					this.body.velocity.y = this._squareDirection[1] * Consts.SQUARE_SPEED;

					var sensorId;
					if (this._squareDirection[0] == 0) {
						sensorId = this._squareDirection[1] == 1 ? 3 : 1;
					}
					else {
						sensorId = this._squareDirection[0] == 1 ? 2 : 0;
					}

 					if (this._squareSensors[sensorId].isColliding()) {
						this._isSquareMoving = false;
						this.alpha = 1;
 					}
				}
				else {
					if (this.body.velocity.x != 0) {
						this.body.velocity.x = 0;
						this.body.velocity.y = 0;
						this.body.x = Math.round(this.body.x / Consts.TILESIZE) * Consts.TILESIZE + 1;
						this.body.y = Math.round(this.body.y / Consts.TILESIZE) * Consts.TILESIZE + 1;
					}
				}
			}

			else if (this._isTriangle) {
				this.body.angularVelocity = 8;
				if (this.body.x > Consts.WIDTH/Consts.PIXEL_SCALE - this.width) {
					this.destroySensors();
					this.destroy();
				}
			}

			/*else {
				console.log("---");
				this._circleSensors.forEach(function(sensor) {
					console.log(sensor._name + ", " +sensor.isColliding())
				});
			}*/
		}

		entity.goLeft = function(pressedSince) {
			if (this._isCircle) {
				var speedRatio = Math.min(1, 1. * (state.time.time - pressedSince) / Consts.CIRCLE_ACCELERATION);
				this.body.moveLeft(Consts.CIRCLE_SPEED * speedRatio);
				this.body.rotateLeft(Consts.CIRCLE_SPEED);
			}
			else if (this._isSquare && !this._isSquareMoving) {
				this._setSquareDirection(-1,0);
			}
		}

		entity.goRight = function(pressedSince) {
			if (this._isCircle) {
				var speedRatio = Math.min(1, 1. * (state.time.time - pressedSince) / Consts.CIRCLE_ACCELERATION);
				this.body.moveRight(Consts.CIRCLE_SPEED * speedRatio);
				this.body.rotateRight(Consts.CIRCLE_SPEED);
			}
			else if (this._isSquare && !this._isSquareMoving) {
				this._setSquareDirection(1,0);
			}
		}

		entity.goUp = function(state) {
			if (this._isCircle) {
				if (this._isColliding(state, 0, 1)) {
					this.body.moveUp(Consts.CIRCLE_JUMP_SPEED);
					return true;
				}
			}
			else if (this._isSquare && !this._isSquareMoving) { // TODO  && !this._squareSensors[sensorId].isColliding()
				this._setSquareDirection(0,-1);
			}
			return false;
		}

		entity.goDown = function() {
			if (this._isSquare && !this._isSquareMoving) {
				this._setSquareDirection(0,1);
			}
		}

		entity.shapeShift = function() {
			var success = false;
			if (entity._canShapeShift) {
				if (this._isCircle) {
					if (this.prepareTurnToSquare()) {
						this.setType('square');
						success = true;
					}
				}
				else if (this._isSquare) {
					this.setType('circle');
					success = true;
				}
			}

			if (success) {
				Particles.playImplosion(state, this);
			}
			else {
				this.tint = 0xffaaaa;
				setTimeout(function() {
					entity.tint = 0xffffff;
				}, 100);
			}

			return success;
		}

		entity.prepareTurnToSquare = function() {
			// Check that 2 non-opposite sides are free
			var dx = 0, dy = 0, success = false;
			for (var i = 0; i < entity._circleSensors.length; i++) {
				var sensor = entity._circleSensors[i];
				if (!sensor.isColliding()) {
					var neighbor = (i+1) % entity._circleSensors.length;
					if (!entity._circleSensors[neighbor].isColliding()) {
						success = true;
					}
				}
				else {
					dx += sensor._dx;
					dy += sensor._dy;
				}
			}

			// Before switching, make sure we're not in a wall
			if (success) {
				entity.body.x -= dx;
				entity.body.y -= dy;
			}

			return success;

		}

		entity.trigger = function(playerBody, otherBody) {
			// Reset on spike hit
			if (otherBody._isSpike && !this._isTriangle
					&& (this.body.x != entity._spawnX || this.body.y != entity._spawnY 
						/* otherwise death is triggered 1 to 4 times*/)) {
				Particles.playExplosion(state, this.x, this.y);
				this.reset();
			}

			// Win
			if (otherBody._isEnd) {
				this._hasFinishedLevel = true;
			}

			// Reveal text
			if (otherBody._isReveal) {
				state._specialText.visible = true;
			}
		}

		entity.reset = function() {
			this.body.x = entity._spawnX;
			this.body.y = entity._spawnY;
			this.body.velocity.x = 0;
			this.body.velocity.y = 0;
			this._isSquareMoving = false;
			this.alpha = 1.0;
			this.setType(entity._spawnType);
		}

		entity.hasFinishedLevel = function() {
			return !!this._hasFinishedLevel;
		}

		entity._setSquareDirection = function(xDirection, yDirection) {
			this._squareDirection = [xDirection, yDirection];
			this._isSquareMoving = true;
			this.alpha = 0.8;
		}

	    entity._isCollidingAnything = function(state) {
	        var result = false;

	        for (var i=0; i < state.physics.p2.world.narrowphase.contactEquations.length; i++) {
	            var c = state.physics.p2.world.narrowphase.contactEquations[i];
	    
	            if (c.bodyA === this.body.data || c.bodyB === this.body.data) {
	                result = true;
	            }
	        }
	        return result;
	    }

	    entity._isColliding = function(state, xDirection, yDirection) {
	        var axis = p2.vec2.fromValues(xDirection, yDirection);
	        var result = false;
	    
	        for (var i=0; i < state.physics.p2.world.narrowphase.contactEquations.length; i++) {
	            var c = state.physics.p2.world.narrowphase.contactEquations[i];
	    
	            if (c.bodyA === this.body.data || c.bodyB === this.body.data) {
	                var d = p2.vec2.dot(c.normalA, axis);
	                if (c.bodyA === this.body.data) {
	                    d *= -1;
	                }
	                if (d > 0.5) {
	                    result = true;
	                }
	            }
	        }
	        return result;
	    }

	    entity._onPostBroadphase = function(bodyA, bodyB) {
	    	var letItCollide = true;
	    	var sensors = entity._isCircle ? entity._circleSensors : entity._squareSensors;
	    	sensors.forEach(function (sensor) {
	    		if ((bodyA == sensor.body || bodyB == sensor.body)
	    			&& bodyA != entity.body && bodyB != entity.body) {
	    			letItCollide = sensor.onMapCollision(bodyA, bodyB);
	    		}
	    	});
	    	return letItCollide;
	    }

		// Init
		state.physics.p2.enable(entity, Consts.DEBUG.SHOW_OBJECT_BODIES);
		entity.setType(type, state);
		if (type == 'circle') {
			entity.body.x += Consts.TILESIZE/2;
			entity.body.y += Consts.TILESIZE/2 - 1;
		}
		else {
			entity.body.x += Consts.TILESIZE + 1;
		}

		entity._spawnType = entity._type;
		entity._spawnX = entity.body.x;
		entity._spawnY = entity.body.y;

		entity._circleSensors = [
			Sensor.create(state, entity, "left", -Consts.TILESIZE/2, 0, Consts.TILESIZE / 2.5, Consts.TILESIZE / 2.5),
			Sensor.create(state, entity, "up", 0, -Consts.TILESIZE/2, Consts.TILESIZE / 2.5, Consts.TILESIZE / 2.5),
			Sensor.create(state, entity, "right", Consts.TILESIZE/2, 0, Consts.TILESIZE / 2.5, Consts.TILESIZE / 2.5),
			Sensor.create(state, entity, "down",0, Consts.TILESIZE/2, Consts.TILESIZE / 2.5, Consts.TILESIZE / 2.5)
		]

		entity._squareSensors = [
			Sensor.create(state, entity, "left", -Consts.TILESIZE, 0, Consts.TILESIZE / 5, Consts.TILESIZE * 1.6),
			Sensor.create(state, entity, "up", 0, -Consts.TILESIZE, Consts.TILESIZE * 1.6, Consts.TILESIZE / 5),
			Sensor.create(state, entity, "right", Consts.TILESIZE, 0, Consts.TILESIZE / 5, Consts.TILESIZE * 1.6),
			Sensor.create(state, entity, "down",0, Consts.TILESIZE, Consts.TILESIZE * 1.6, Consts.TILESIZE / 5)
		]

		if (type != 'triangle') {
			state.physics.p2.setPostBroadphaseCallback(entity._onPostBroadphase, entity);
		}

		return entity;

	}

	var circleMaterial = null;
	var squareMaterial = null;

	function getCircleMaterial(state, wallsMaterial) {
		if (!circleMaterial) {
			circleMaterial = state.physics.p2.createMaterial('circleMaterial');

			var contact = state.physics.p2.createContactMaterial(circleMaterial, wallsMaterial);
			contact.friction = 0.3;     // Friction to use in the contact of these two materials.
		    contact.restitution = 0;  // Restitution (i.e. how bouncy it is!) to use in the contact of these two materials.
		    contact.stiffness = 1e7;    // Stiffness of the resulting ContactEquation that this ContactMaterial generate.
		    contact.relaxation = 3;     // Relaxation of the resulting ContactEquation that this ContactMaterial generate.
		    contact.frictionStiffness = 1e7;    // Stiffness of the resulting FrictionEquation that this ContactMaterial generate.
		    contact.frictionRelaxation = 3;     // Relaxation of the resulting FrictionEquation that this ContactMaterial generate.
		    contact.surfaceVelocity = 0;        // Will add surface velocity to this material. If bodyA rests on top if bodyB, and the surface velocity is positive, bodyA will slide to the right.
		}
		return circleMaterial;
	}

	function getSquareMaterial(state, wallsMaterial) {
		if (!squareMaterial) {
			squareMaterial = state.physics.p2.createMaterial('squareMaterial');

			var contact = state.physics.p2.createContactMaterial(squareMaterial, wallsMaterial);
			contact.friction = 1;     // Friction to use in the contact of these two materials.
		    contact.restitution = 0;  // Restitution (i.e. how bouncy it is!) to use in the contact of these two materials.
		    contact.stiffness = 1e7;    // Stiffness of the resulting ContactEquation that this ContactMaterial generate.
		    contact.relaxation = 3;     // Relaxation of the resulting ContactEquation that this ContactMaterial generate.
		    contact.frictionStiffness = 1e7;    // Stiffness of the resulting FrictionEquation that this ContactMaterial generate.
		    contact.frictionRelaxation = 3;     // Relaxation of the resulting FrictionEquation that this ContactMaterial generate.
		    contact.surfaceVelocity = 0;        // Will add surface velocity to this material. If bodyA rests on top if bodyB, and the surface velocity is positive, bodyA will slide to the right.
		}
		return squareMaterial;
	}

});
