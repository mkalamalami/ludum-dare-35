define(['consts'], function(Consts) {

	return {
		create: create
	};

	function create(state, player, name, dx, dy, width, height) {

		var entity = state.add.sprite(player.x, player.y);
		entity.width = width;
		entity.height = height;
		entity._isColliding = false;
		entity._name = name;
		entity._dx = dx;
		entity._dy = dy;

		entity.update = function() {
			entity.body.x = player.body.x + dx;
			entity.body.y = player.body.y + dy;

			if (entity.body.x <= 2 || entity.body.y <= 2
				|| entity.body.x >= state.world.bounds.width-2
				|| entity.body.y >= state.world.bounds.height-2) {
				this._worldCollision = true;
			}
			else {
				this._worldCollision = false;
			}
		}

		entity.onMapCollision = function(entityBody, mapBody) {
			if (entityBody._isMap || mapBody._isMap) {
				entity._collidingTime = state.time.time;
			}
			return false;
		}

		entity.isColliding = function() {
			return this._worldCollision ||
				Math.abs(entity._collidingTime + state.time.physicsElapsedMS - state.time.time) < state.time.physicsElapsedMS; // XXX Hacky
		}

		state.physics.p2.enable(entity, Consts.DEBUG.SHOW_OBJECT_BODIES);
	//	var shape = entity.body.setRectangle(entity.width, entity.height, 0, 0, 0);
	//	shape.sensor = true;
		entity.body.setCollisionGroup(state._groups.sensors);
		entity.body.collides(state._groups.map);

		entity.body.debug = false;

		// DEBUG
 		//entity.body.debug = Consts.DEBUG.SHOW_OBJECT_BODIES; // XXX debug body falls down...
		//entity.body.static = true; // XXX no more collisions but fixes the debug body...

		return entity;

	}

});
