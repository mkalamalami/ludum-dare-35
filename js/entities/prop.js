define(['consts'], function(Consts) {

	return {
		create: create
	};

	function create(state, color, x, y) {

		var entity = state.add.sprite(x, y, 'tileset', color+31);
		entity.scale.set(0.9);

		entity.randomizeSpeed = function() {
			entity.body.velocity.x = randomSpeed();
			entity.body.velocity.y = randomSpeed();
			entity.body.angularVelocity = randomSpeed();
		}

		state.physics.p2.enable(entity, Consts.DEBUG.SHOW_OBJECT_BODIES);
		entity.body.setCollisionGroup(state._groups.props);
		entity.body.collides(state._groups.props);
		entity.body.collides(state._groups.map);
		entity.body.x += Consts.TILESIZE / 2;
		entity.body.y += Consts.TILESIZE / 2;
		entity.randomizeSpeed();

		return entity;

	}

	function randomSpeed() {
		return 3 * (Math.random() - .5);
	}

});
