define(['consts'], function(Consts) {

return {

	images: [
		'background_1',
		'background_2',
		'background_3',
		'background_4',
		'background_5',
		'background_6',
		'square',
		'circle',
		'triangle',
		'particle'
	],
	audio: [
		//'name',
	],
	spritesheet: [
		['tileset', Consts.TILESIZE, Consts.TILESIZE],
	]

};

});
