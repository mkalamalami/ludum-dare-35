define(['consts', 'engine/save-state'],
		function(Consts, SaveState) {

	var state = new Phaser.State();

	state.create = function() {
		if (!SaveState.level || Consts.DEBUG.SKIP_RESUME) {
			this.state.start('intro');
			return;
		}

		state.add.bitmapText(10, 10, 'visitor16', 'Continue existing save? (y/n)', 16);
	};

	state.update = function() {
		if (this.input.keyboard.isDown(Phaser.Keyboard.Y)) {
			this.state.start('level');
		}
		if (this.input.keyboard.isDown(Phaser.Keyboard.N)) {
			SaveState.playedIntro = false;
			SaveState.level = 1;
			this.state.start('intro');
		}
	};

	return state;

});
